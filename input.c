#include <libguile.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/select.h>
#include <termios.h>
#include <stdio.h>



//static SCM input = 0;
static int directions[] = {'w','s','a','d'};

struct termios orig_termios;


void reset_terminal_mode()
{
  tcsetattr(0, TCSANOW, &orig_termios);
}

void set_conio_terminal_mode()
{
  struct termios new_termios;

  tcgetattr(0, &orig_termios);
  memcpy(&new_termios, &orig_termios, sizeof(new_termios));

  atexit(reset_terminal_mode);
  cfmakeraw(&new_termios);
  tcsetattr(0, TCSANOW, &new_termios);
  
}

int kbhit()
{
  struct timeval tv = {0L, 0L};
  fd_set fds;
  FD_ZERO(&fds);
  FD_SET(0, &fds);
  return select(1, &fds, NULL, NULL, &tv) >0;
  
}
unsigned char getch()
{
  int r;
  unsigned char c;
  if ((r = read(0, &c, sizeof(c))) > 0)
    {
      return (int)c;
    } else
    {
      return r;
    }
}
int input_filter(int input, SCM valid_input_type)
{
  int* arr = NULL;
  int check = 0;
  switch (scm_to_int(valid_input_type)){
  case 0:
    arr = directions;
  };
  for(size_t index = 0; index < sizeof(arr); ++index)
    {
      if(input == arr[index])
	{ check = 1; break;}
    }
  if (check ==1){return input;} else{return -1;};
}

static SCM  input_loop(SCM condition, SCM valid_input_type)
{
  SCM input;
  
  set_conio_terminal_mode();
  while(input != condition)
    {
      while(!kbhit())
	{
	  int what = 1;
	}
      input = scm_from_int(getch());
      if (input_filter(scm_to_int(input), valid_input_type) != -1){break;}
    }
  reset_terminal_mode();
  return input;
}


static void* register_functions(void *data)
{
  scm_c_define_gsubr("input-loop",2,0,0, &input_loop);

  return NULL;
}

void init_input()
{
    scm_c_define_gsubr("input-loop",2,0,0, &input_loop);
    //    scm_c_define_gsubr("peek-input",0,0,0, &peek_input);
}

int main(int argc, char *argv[]) {  
  scm_with_guile(&register_functions, NULL);
  scm_shell(argc, argv);
}

