CFLAGS = `guile-config compile`
LIBS = `guile-config link`
LFLAGS = -fPIC
COMPILE_FLAGS = -shared -o
PKG_CONFIG = `pkg-config --cflags guile-3.0`

.PHONY: clean build run

build: input

clean:
	rm -f input input.o

run: input
	./input

input: input.o
	gcc $< -o $@ $(LIBS)

input.o: input.c
	gcc -c $< -o $@ $(CFLAGS)

lib: 
	gcc -g $(PKG_CONFIG) $(COMPILE_FLAGS) libgp-input.so $(LFLAGS) input.c
